import numpy as np
import sys
import time

class Grain:

    def __init__(self, key, IV, nfsr_size, number_of_rounds = False):
        self.IV = IV
        self.key = key
        # Generátor predvýstupu
        # inicializácia NFSR
        self.NFSR = key.copy()[:nfsr_size]
        #size at leas 5
        self.fsr_size = nfsr_size
        # inicializácia LFSR
        self.LFSR = IV[:nfsr_size]
        #self.LFSR = np.append(self.LFSR, np.ones(31, dtype = int), axis=None)
        #self.LFSR = np.append(self.LFSR,0, axis=None)
        #Initialize
        A_R_round = int(self.fsr_size/2)
        if number_of_rounds == False:
            self.round_init(self.fsr_size*3 - int(self.fsr_size/2))
            self.round_init_with_key(int(self.fsr_size/2))
            self.no_of_rounds = self.fsr_size*3 + 2*int(self.fsr_size/2)
        else:
            n = int(number_of_rounds/4)
            self.round_init(n*3 - int(n/2))
            self.round_init_with_key(int(n/2))
            A_R_round = int(n/2)
            self.no_of_rounds = n*3 + 2*(int(n/2))
        # Initialize shif register R and accumulator A
        self.A = np.array([], dtype='int16')
        self.R = np.array([], dtype='int16')
        for i in range(A_R_round):
            self.A = np.append(self.A, self.round_())
        for i in range(A_R_round):
            self.R = np.append(self.R, self.round_())

    def update_bit_lfsr(self):
        # f(x) = 1 + x^32 + x^47 + x^58 + x^90 + x^121 + x^128 */
        # whole indexes [96,81,70,38,7,0]
        indexes =  np.array([96,81,70,38,7,0])
        indexes = self.delete_indexes_1D(indexes, self.fsr_size)
        next_bit = self.LFSR[indexes.pop(0)]
        for i in indexes:
            next_bit ^= self.LFSR[i]
        return next_bit

    def delete_indexes_1D(self,indexes,n):
        temp = []
        for index in indexes:
            if index < n:
                temp.append(index)
        return temp

    def update_bit_nfsr(self):
        indexes =  np.array([np.array([88,92,93,95]),np.array([70,78,82]),np.array([22,24,25]),np.array([68,84])
                            ,np.array([61,65]),np.array([40,48]),np.array([27,59]),np.array([17,18]),np.array([11,13]),np.array([3,67])
                            ,np.array([96]),np.array([91]),np.array([56]),np.array([26]),np.array([0])], dtype=object)
        indexes = self.delete_indexes_MD(indexes,self.fsr_size) #keep only indexes < self.fsr_size
        next_bit = self.LFSR[0]
        for indx in indexes:
            value = self.NFSR[indx[0]]
            indx = np.delete(indx, 0)
            if(len(indx) != 0):
                for indy in indx:
                    value &= self.NFSR[indy]
            next_bit ^= value
        return next_bit

    def delete_indexes_MD(self,indexes,n):
        temp1 = []
        for arr in indexes:
            temp2 = []
            for index in arr:
                if index < n:
                    temp2.append(index)
            temp1.append(temp2)

        temp = []
        for arr in temp1:
            if arr:
                temp.append(arr)
        return temp

    # Shift register FSR = True --> LFSR //// FSR = False --> NFSR
    def shift_fsr(self, FSR, next_bit):
        if(FSR):
            self.LFSR = np.roll(self.LFSR,-1)
            self.LFSR[(self.fsr_size-1)] = next_bit
        else:
            self.NFSR = np.roll(self.NFSR,-1)
            self.NFSR[(self.fsr_size-1)] = next_bit

    # Boolean function h
    def function_h(self):
        h = self.NFSR[12]&self.LFSR[8]
        # size -1 because max index is 127 and size of SFR is 128
        size = self.fsr_size-1
        if(20 <= size):
            h ^= self.LFSR[13]&self.LFSR[20]
        if(79 <= size):
            h ^= self.LFSR[60]&self.LFSR[79]
        if(95 <= size):
            h ^= self.NFSR[12]&self.NFSR[95]&self.LFSR[94]
        if(95 <= size):
            h ^= self.NFSR[95]&self.LFSR[42]
        return h

    # Pre-output function
    def function_y(self):
        # whole indexes [2,15,36,45,64,73,89]
        indexes = np.array([15,36,45,64,73,89])
        #keep values below max exponent
        indexes = indexes[indexes < self.fsr_size]
        sum = self.NFSR[2]
        for i in indexes:
            sum ^= self.NFSR[i]
        result = (self.function_h()^sum)
        if 93 <= (self.fsr_size-1):
            (result^self.LFSR[93])
        return result

    def round_init(self, number_of_rounds):
        for i in range(number_of_rounds):
            next_bit_lfsr = self.update_bit_lfsr()
            next_bit_nfsr = self.update_bit_nfsr()
            y = self.function_y()

            self.shift_fsr(False,next_bit_nfsr^y)
            self.shift_fsr(True,next_bit_lfsr^y)

    def round_init_with_key(self, number_of_rounds):
        for i in range(0,number_of_rounds):
            next_bit_lfsr = self.update_bit_lfsr()
            next_bit_nfsr = self.update_bit_nfsr()
            y = self.function_y()

            self.shift_fsr(False,next_bit_nfsr^y^self.key[(i + int(self.fsr_size/2))])
            self.shift_fsr(True,next_bit_lfsr^y^self.key[i])

    def round_(self):
        next_bit_lfsr = self.update_bit_lfsr()
        next_bit_nfsr = self.update_bit_nfsr()
        y = self.function_y()

        self.shift_fsr(False,next_bit_nfsr)
        self.shift_fsr(True,next_bit_lfsr)
        return y

    def print_init_state(self):
        print("LFSR:")
        print(f"    {self.LFSR}")
        print(f"    L:{len(self.LFSR)}")
        print("---------------------------")
        print("NFSR:")
        print(f"    {self.NFSR}")
        print(f"    L:{len(self.NFSR)}")
        print("---------------------------")
        #print("SR:")
        #print(f"    {self.SR}")
        #print(f"    L:{len(self.SR)}")
        #print("---------------------------")
        #print("ACC:")
        #print(f"    {self.ACC}")
        #print(f"    L:{len(self.ACC)}")

    def print_nfsr(self):
        for i in range(len(self.NFSR)):
            print (f"{i} : {self.NFSR[i]}")
        print("---------------------------")

    def generate_keystream(self,len):
        result = []
        for i in range(len):
            result.append(grain.round_())
        with open(f'equations/keystream_GRAIN_{self.fsr_size}_{self.no_of_rounds}.txt', "w+") as f:
            f.write('%s\n' %result)
        f.close()

if __name__ == "__main__":


    #key = np.array([1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1,
    #0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0,
    #1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1,
    #0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0])

    #IV = np.array([0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0,
    #1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1,
    #1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1,
    #0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1])

    bit_version = int(sys.argv[1])
    number_of_rounds = int(sys.argv[2])

    #key = np.random.choice(a=[1,0], size=bit_version)
    #key = np.array([1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0])
    key = np.array([1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0,])
    IV = np.random.choice(a=[1,0], size=bit_version)
    #IV = np.array([0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0])

    print(key)
    with open('nonce.txt', "w+") as f:
        f.write('%s\n' %IV)
    f.close()
    grain = Grain(key, IV, bit_version, number_of_rounds)
    grain.generate_keystream(16)
