import numpy as np
import sys
import os

def read_equations(bit_versionn, no_of_rounds):
    Polynomials = []
    with open(f'equations/equations_GRAIN_{bit_version}_{no_of_rounds}.txt') as f:
        equations = f.readlines()
    f.close()
    equations = list(map(str.strip, equations[0][1:-2].strip().split(",")))
    return equations

def read_equations_LSH(bit_versionn, no_of_rounds):
    Polynomials = []
    with open(f'equations/equations_GRAIN_{bit_versionn}_{no_of_rounds}.txt') as f:
        equations = f.readlines()
    f.close()
    equations = list(map(str.strip, equations[0][1:-2].strip().replace("'","").replace("\\","").replace('"',"").split(",")))
    return equations

bit_version = 16
number_of_rounds = 16

n = int(number_of_rounds/4)
no_of_rounds = n*3 + 2*(int(n/2))

equations_time =  []
equations_memory = []
magma_time = []
magma_time_script = []
magma_memory = []
num_of_max_monoms = []
num_of_min_monoms = []
num_of_keys = []
key_correctness = []

for i in range(1):
    for i in range(2):
        os.system(f'sage GrainPolynomial.sage {bit_version} {number_of_rounds}')

    f = open(f'time/eq_time_GRAIN_{bit_version}_{no_of_rounds}.txt')
    f_read = f.readlines()
    equations_time.append(float(f_read[0][0:-2]))
    equations_memory.append(float(f_read[1][0:-2]))
    f.close()
    #print(equations_time[i])
    os.system(f'python3 MagmaSolver.py {bit_version} {number_of_rounds}')

    f1 = open(f'time/magma_time_GRAIN_{bit_version}_{no_of_rounds}.txt')
    f1_read = f1.readlines()
    f2 = open(f'time/magma_memory_GRAIN_{bit_version}_{no_of_rounds}.txt')
    f2_read = f2.readlines()

    res = 0
    if len(f2_read[1].split("/")) == 2:
        res = float((f2_read[1][0:-1]).split("/")[0])/float((f2_read[1][0:-1]).split("/")[1])
    else:
        res = float(f2_read[1])
    magma_memory.append(float(f1_read[1][0:-2]) + res)
    magma_time.append(float(f1_read[0][0:-2]))
    magma_time_script.append(float(f2_read[0][0:-2]))
    f2.close()
    f1.close()
    LSH = True
    polynomials = []
    if LSH == True:
        polynomials = read_equations_LSH(bit_version, no_of_rounds)
    else:
        polynomials = read_equations(bit_version, no_of_rounds)
    num_of_min_monoms.append(len(list(map(str.strip, (polynomials[0]).strip().split("+")))))
    num_of_max_monoms.append(len(list(map(str.strip, (polynomials[-1]).strip().split("+")))))
    print(f'Dlzka keystreame je: {len(polynomials)}')
    f = open(f'key_correctness.txt')
    f_read = f.readlines()
    num_of_keys.append(int(f_read[0]))
    key_correctness.append(f_read[1])
    f.close()
    with open(f'help.txt', "w+") as f:
        f.write('%s\n' %str(equations_time))
        f.write('%s\n' %str(equations_memory))
        f.write('%s\n' %str(magma_time))
        f.write('%s\n' %str(magma_memory))
        f.write('%s\n' %str(num_of_min_monoms))
        f.write('%s\n' %str(num_of_max_monoms))
    f.close()
results = (
            '\nGen. equations--------------------\n'
            f'  Mean of time (s): {np.mean(equations_time)}\n'
            f'  Standart deviation of time: {np.std(equations_time)}\n'
            f'  Mean of memory (MB): {np.mean(equations_memory)}\n'
            f'  Standart deviation of memory: {np.std(equations_memory)}\n'
            f'  Mean of min monoms in one polynomial: {np.mean(num_of_min_monoms)}\n'
            f'  Mean of max monoms in one polynomial: {np.mean(num_of_max_monoms)}\n'
            f'  Standart deviation of min monoms in one polynomial: {np.std(num_of_min_monoms)}\n'
            f'  Standart deviation of max monoms in one polynomial: {np.std(num_of_max_monoms)}\n'
            f'  Mean of broken keys: {np.mean(num_of_keys)}\n'
            f'  Standart deviation of broken keys: {np.std(num_of_keys)}\n'
            f'  {key_correctness}\n'
            'Magma--------------------\n'
            f'  Mean of time (only script): {np.mean(magma_time_script)}\n '
            f'  Standart deviation of time (only script): {np.std(magma_time_script)}\n'
            f'  Mean of time (s): {np.mean(magma_time)}\n'
            f'  Standart deviation of time: {np.std(magma_time)}\n'
            f'  Mean of memory (MB): {np.mean(magma_memory)}\n'
            f'  Standart deviation of memory: {np.std(magma_memory)}\n'
)
print(results)
