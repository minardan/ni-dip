from sage.interfaces.magma import magma
import numpy as np
import time
import os
import psutil
import sys

from Grain import Grain

class MagmaSolver:

    def __init__(self, bit_version, number_of_rounds = False):
        self.in_number_of_rounds = number_of_rounds
        self.BIT_VERSION = bit_version
        if number_of_rounds == False:
            self.no_of_rounds = self.BIT_VERSION*3 + 2*int(self.BIT_VERSION/2)
        else:
            n = int(number_of_rounds/4)
            self.no_of_rounds = n*3 + 2*(int(n/2))

        self.Polynomials = self.read_equations()

    def read_equations(self):
        Polynomials = []
        with open(f'equations/equations_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.txt') as f:
            equations = f.readlines()
        f.close()
        equations = list(map(str.strip, equations[0][1:-2].strip().split(",")))
        return equations

    def split_polynomial(self,polynomial):
        polynom_split = list(map(str.strip, polynomial.strip().split("+")))
        num_of_splits = len(polynom_split)//10000
        rest_of_monoms = len(polynom_split) - num_of_splits*10000
        polynom_split_result = []
        for i in range(num_of_splits):
            polynom_split_result.append(" + ".join(polynom_split[i*10000:(i+1)*10000]))
        if(rest_of_monoms != 0):
            polynom_split_result.append(" + ".join(polynom_split[num_of_splits*10000:(len(polynom_split))]))
        return polynom_split_result

    def create_magma_script(self):
        with open(f'variables.txt') as f:
            variables = f.readlines()[0]
        f.close()
        magma_file = open(f'magma_scripts/magma_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.in', "w+")
        magma_file.write('%s\n' %f'SetOutputFile("magma_outputs/magma_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.out": Overwrite := true);')
        magma_file.write('%s\n' %f'P<{variables}> := BooleanPolynomialRing({self.BIT_VERSION},"grevlex");')
        magma_file.write('%s\n' %f'T := Time();')

        magma_eq = 'B := ['
        for j in range(len(self.Polynomials)):
            polynom = self.split_polynomial(self.Polynomials[j])
            magma_poly = f'P{j} := '
            for i in range(len(polynom)):
                if i == 0:
                    magma_poly += f'P{j}_{i}'
                else:
                    magma_poly += f' + P{j}_{i}'

                magma_file.write('%s\n' %f'P{j}_{i} := {polynom[i]};')
            magma_poly += ';'
            magma_file.write('%s\n' %magma_poly)
            if j == 0:
                magma_eq += f'P{j}'
            else:
                magma_eq += f', P{j}'
        magma_eq +='];'
        magma_file.write('%s\n' %magma_eq)
        magma_end = (
                      f'gb := GroebnerBasis(B);\n'
                      f'Variety(Ideal(gb));\n'
                      'UnsetOutputFile();\n'
                      f'SetOutputFile("time/magma_memory_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.txt": Overwrite := true)\n;'
                      'Time(T);\n'
                      'GetMaximumMemoryUsage()/1024/1024;\n'
                      'UnsetOutputFile();\n'
                      'quit;'
        )
        magma_file.write('%s\n' %magma_end)

    def read_nonce(self):
        nonce = []
        with open(f'nonce.txt') as f:
            nonce = "".join(f.readlines()).replace("\n", "").replace("[","").replace("]","").split(" ")
        f.close()
        result = list(map(int, nonce))
        return result

    def correctness_of_keys(self):
        magma_output = open(f'magma_outputs/magma_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.out')
        keys_output = "".join(magma_output.readlines()).replace(" ", "").replace("\n", "").replace("[","").replace("]","")
        magma_output.close()
        real_keystream = []
        with open(f'equations/keystream_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.txt') as f:
            real_keystream = "".join(f.readlines()).replace("\n", "").replace("[","").replace("]","").split(",")
        f.close()
        real_keystream = list(map(int, real_keystream))
        i = 0
        list_keys = []
        rng =  self.BIT_VERSION*2 + 1
        while i <= len(keys_output):
            list_keys.append(keys_output[i:i+rng])
            i += rng+1
        nonce = self.read_nonce()
        counter = 0
        for key in list_keys:
            key_num = list(map(int, key.replace("<", "").replace(">", "").split(",") ))
            grain = Grain(key_num, nonce, self.BIT_VERSION, self.in_number_of_rounds)
            keystream = []
            for i in range(len(real_keystream)):
                keystream.append(grain.round_())
            if keystream != real_keystream:
                print("This key is wrong")
                print(key)
                print("-----------------")
            else:
                counter+=1
        key_correctness = open(f'key_correctness.txt', "w+")
        key_correctness.write('%s\n' %(len(list_keys)))
        print(len(list_keys))
        if counter == len(list_keys) and len(list_keys) != 0:
            #print("All keys are correct")
            key_correctness.write('%s\n' %("True"))
        else:
            key_correctness.write('%s\n' %("False"))
        key_correctness.close()

    def break_grain(self):
        self.create_magma_script()
        os.system(f'magma magma_scripts/magma_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.in')
        self.correctness_of_keys()

    def read_equations_LSH(self):
        Polynomials = []
        with open(f'equations/equations_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.txt') as f:
            equations = f.readlines()
        f.close()
        equations = list(map(str.strip, equations[0][1:-2].strip().replace("'","").replace("\\","").replace('"',"").split(",")))
        return equations

    def reduce_LSH(self):
        #reduced polynomials
        psets = 2
        bits = self.BIT_VERSION
        polys = []
        shingles = []
        #allpolys = self.Polynomials
        allpolys = self.read_equations_LSH()
        #return
        for poly in allpolys:
        #poly = self.Polynomials[0]
        #print(poly)
            monomials = list(map(str.strip, poly.strip().split("+")))
            shingles.append(set(monomials))
        ##make dict shingles
        shingles_all = set().union(*shingles)
        shingles_dict = {}
        for i, shingle in enumerate(list(shingles_all)):
            shingles_dict[shingle] = i
        #print(shingles_dict)
            ##PIECES OF CODE ARE TAKEN FROM:
            ##https://www.pinecone.io/learn/locality-sensitive-hashing/
            ##https://github.com/pinecone-io/examples/blob/master/locality_sensitive_hashing_traditional/testing_lsh.ipynb

        ##make bin vectors for polynomials from shingles_dict
        allpolys_vec = []
        for shingle_set in shingles:
            vec = np.zeros(len(shingles_dict))
            for shingle in shingle_set:
                idx = shingles_dict[shingle]
                vec[idx] = 1
            allpolys_vec.append(vec)
        allpolys_vec = np.stack(allpolys_vec)
        #print(allpolys_vec)

        if allpolys_vec.shape[1] < 1000:
            resolution = 50
            b=10
        elif allpolys_vec.shape[1] < 10000:
            resolution = 100
            b=20
        else:
            resolution = 500
            b=100
        ##minhash
        len_dict = len(shingles_dict.keys())
        minhash = np.zeros((resolution, len_dict))
        for i in range(resolution):
            permutation = np.random.permutation(len(shingles_dict)) + 1
            minhash[i, :] = permutation.copy()
        minhash = minhash.astype(int)
        #print(minhash)
        signatures = []
        for vector in allpolys_vec:
            # get index locations of every 1 value in vector
            idx = np.nonzero(vector)[0].tolist()
            # use index locations to pull only +ve positions in minhash
            shingles_minhash = minhash[:, idx]
            # find minimum value in each hash vector
            signature = np.min(shingles_minhash, axis=1)
            signatures.append(signature)

        signatures = np.stack(signatures)
        #print(signatures)
####################################################################################
        from itertools import combinations
        class LSH:
            buckets = []
            counter = 0
            def __init__(self, b):
                self.b = b
                for i in range(b):
                    self.buckets.append({})

            def make_subvecs(self, signature):
                l = len(signature)
                assert l % self.b == 0
                r = int(l / self.b)
                # break signature into subvectors
                subvecs = []
                for i in range(0, l, r):
                    subvecs.append(signature[i:i+r])
                return np.stack(subvecs)

            def add_hash(self, signature):
                subvecs = self.make_subvecs(signature).astype(str)
                #print(subvecs)
                for i, subvec in enumerate(subvecs):
                    subvec = ','.join(subvec)
                    if subvec not in self.buckets[i].keys():
                        self.buckets[i][subvec] = []
                    self.buckets[i][subvec].append(self.counter)
                self.counter += 1
                #print(self.buckets)

            def check_candidates(self):
                candidates = []
                #print(self.buckets)
                for bucket_band in self.buckets:
                    keys = bucket_band.keys()
                    for bucket in keys:
                        hits = bucket_band[bucket]
                        #print(hits)
                        if len(hits) > 1:
                            candidates.extend(combinations(hits, 2))
                return set(candidates)
        lsh = LSH(b)
        for sig in signatures:
            lsh.add_hash(sig)
        candidate_pairs = lsh.check_candidates()
        print("------>Candidate")
        print(candidate_pairs)
        print(len(candidate_pairs))
        sorted_candidates=[]
        print(f'Celkovy pocet polynomov {len(allpolys)}')
        for pair in list(candidate_pairs):
            self.poly_add(allpolys[pair[0]],allpolys[pair[1]])
            with open(f'magma_help.out') as f:
                equation = f.read().splitlines()
            f.close()
            equation = " + ".join("".join(equation).replace(" ","").split("+"))
            d = len(equation)
            sorted_candidates.append((d,pair[0],pair[1]))
        sorted_candidates.sort()

        ##take xor of best pairs
        count=0
        i=0
        rng = 0
        if (len(candidate_pairs) > 16):
            rng = 16
        else:
            rng = len(candidate_pairs)

        for k in range(rng):
            if sorted_candidates[i][0] == 0:
                i+=1
                continue
            self.poly_add(allpolys[sorted_candidates[i][1]],allpolys[sorted_candidates[i][2]])
            with open(f'magma_help.out') as f:
                equation = f.read().splitlines()
            f.close()
            equation = " + ".join("".join(equation).replace(" ","").split("+"))
            polys.append(equation)
            os.system(f'rm magma_help.out')
            count+=1
            i+=1
#        for i in range(self.BIT_VERSION - len(candidate_pairs)):
#            polys.append(self.Polynomials[i])
        self.Polynomials = polys
        with open(f'equations/equations_GRAIN_{self.BIT_VERSION}_{self.no_of_rounds}.txt', "w+") as f:
            f.write('%s\n' %f'{polys}')
        f.close()
#        lng = []
#        for poly in self.Polynomials:
#            lng.append(len(poly.split("+")))
#        lng.sort()
#        print(lng[0])

    def poly_add(self,poly1,poly2):
        with open(f'variables.txt') as f:
            variables = f.readlines()[0]
        f.close()

        magma_file = open(f'magma_help.in', "w+")
        magma_file.write('%s\n' %f'SetOutputFile("magma_help.out": Overwrite := true);')
        magma_file.write('%s\n' %f'P<{variables}> := BooleanPolynomialRing({self.BIT_VERSION},"grevlex");')

        polynoms = []
        polynoms.append(self.split_polynomial(poly1))
        polynoms.append(self.split_polynomial(poly2))
        for j in range(len(polynoms)):
            polynom = polynoms[j]
            magma_poly = f'P{j} := '
            for i in range(len(polynom)):
                if i == 0:
                    magma_poly += f'P{j}_{i}'
                else:
                    magma_poly += f' + P{j}_{i}'

                magma_file.write('%s\n' %f'P{j}_{i} := {polynom[i]};')
            magma_poly += ';'
            magma_file.write('%s\n' %magma_poly)
        f = (f'P0 + P1;\n'
            f'UnsetOutputFile();\n'
            f'quit;')
        magma_file.write('%s\n' %f)
        magma_file.close()
        os.system(f'magma magma_help.in')
        os.system(f'rm magma_help.in')

if __name__ == "__main__":
    startTime = time.time()
    bit_version = int(sys.argv[1])
    number_of_rounds = int(sys.argv[2])
    magma_solver = MagmaSolver(bit_version, number_of_rounds)
    magma_solver.reduce_LSH()
    magma_solver.break_grain()
    if number_of_rounds == False:
        number_of_rounds = bit_version*3 + 2*int(bit_version/2)
    else:
        n = int(number_of_rounds/4)
        number_of_rounds = n*3 + 2*(int(n/2))

    process = psutil.Process(os.getpid())
    executionTime = (time.time() - startTime)
    with open(f'time/magma_time_GRAIN_{bit_version}_{number_of_rounds}.txt' , "w+") as f:
        f.write('%s\n' %str(executionTime))
        f.write('%s\n' %str(float(process.memory_info()[0]/ 1024 ** 2)))
    f.close()
    #print(equation)
    #print(len(magma_solver.Polynomials[0].split("+")))
    #print(len(magma_solver.Polynomials[15].split("+")))
    #print(len(equation.split("+")))
