from sage.interfaces.magma import magma
import numpy as np
import copy
import time
import os
import psutil
import sys

class GrainPolynomial:

    def __init__(self, fsr_size, number_of_rounds = False):
        self.LFSR_num = self.read_nonce()
        #self.LFSR_num = np.append(self.LFSR_num, np.ones(31, dtype = int), axis=None)
        #self.LFSR_num = np.append(self.LFSR_num,0, axis=None)
        self.fsr_size = fsr_size
        self.t = 0
        self.NFSR = list(B.gens())
        self.key = list(B.gens())

        self.LFSR = []
        for i in range(fsr_size):
            self.LFSR.append(self.LFSR_num[i])

        A_R_round = int(self.fsr_size/2)
        if number_of_rounds == False:
            self.round_init(self.fsr_size*3 - int(self.fsr_size/2))
            self.round_init_with_key(int(self.fsr_size/2))
            self.no_of_rounds = self.fsr_size*3 + 2*int(self.fsr_size/2)
        else:
            n = int(number_of_rounds/4)
            self.round_init(n*3 - int(n/2))
            self.round_init_with_key(int(n/2))
            A_R_round = int(n/2)
            self.no_of_rounds = n*3 + 2*int(n/2)

        #Initialize shif register R and accumulator A
        self.A = np.array([], dtype='int16')
        self.R = np.array([], dtype='int16')
        for i in range(A_R_round):
            #print(i+1)
            self.A = np.append(self.A, self.round_())
        for i in range(A_R_round):
            #print(i+1)
            self.R = np.append(self.R, self.round_())

    def update_bit_lfsr(self):
        # f(x) = 1 + x^32 + x^47 + x^58 + x^90 + x^121 + x^128 */
        # whole indexes [96,81,70,38,7,0]
        indexes = [96,81,70,38,7,0]

        #Keep indexes below size
        indexes = self.delete_indexes_1D(indexes, self.fsr_size)
        next_bit = self.LFSR[indexes.pop(0) + self.t]
        for i in indexes:
            next_bit += self.LFSR[i + self.t]
        return next_bit

    def delete_indexes_1D(self,indexes,n):
        temp = []
        for index in indexes:
            if index < n:
                temp.append(index)
        return temp

    def update_bit_nfsr(self):
        indexes = [[88,92,93,95], [70,78,82], [22,24,25], [68,84], [61,65], [40,48],[27,59]
                            ,[17,18], [11,13], [3,67], [96], [91], [56], [26], [0]]
        indexes = self.delete_indexes_MD(indexes,self.fsr_size) #Keep only indexes < self.fsr_size
        next_bit = self.LFSR[0 + self.t]

        for indx in indexes:
            value = self.NFSR[indx[0] + self.t]
            indx.pop(0)
            if(len(indx) != 0):
                for indy in indx:
                    value *= self.NFSR[indy + self.t]
            next_bit += value
        return next_bit

    def delete_indexes_MD(self,indexes,n):
        temp1 = []
        for arr in indexes:
            temp2 = []
            for index in arr:
                if index < n:
                    temp2.append(index)
            temp1.append(temp2)

        temp = []
        for arr in temp1:
            if arr:
                temp.append(arr)
        return temp

    def shift_fsr(self, FSR, next_bit):
        if(FSR):
          self.LFSR.append(next_bit)
        else:
          self.NFSR.append(next_bit)

    def rotate_array(self,list, n):
        return list[n:] + list[:n]

    # Boolean function h
    def function_h(self):
        h = self.NFSR[12 + self.t]*self.LFSR[8 + self.t]
        # size -1 because max index is 127 and size of SFR is 128
        size = self.fsr_size-1
        if(20 <= size):
            h += self.LFSR[13 + self.t] * self.LFSR[20 + self.t]
        if(79 <= size):
            h += self.LFSR[60 + self.t] * self.LFSR[79 + self.t]
        if(95 <= size):
            h += self.NFSR[12 + self.t] * self.NFSR[95 + self.t] * self.LFSR[94 + self.t]
        if(95 <= size):
            h += self.NFSR[95 + self.t] * self.LFSR[42 + self.t]
        return h

    # Pre-output function
    def function_y(self):
        # whole indexes [2,15,36,45,64,73,89]
        indexes = [15,36,45,64,73,89]
        #keep values below max exponent
        indexes = self.delete_indexes_1D(indexes, self.fsr_size)
        sum = self.NFSR[2 + self.t]
        for i in indexes:
            sum += self.NFSR[i + self.t]
        result = (self.function_h() + sum)
        if 93 <= (self.fsr_size-1):
            result + self.LFSR[93 + self.t]
        return result

    def round_init(self, number_of_rounds):
        for i in range(number_of_rounds):
            #print(i+1)
            next_bit_lfsr = self.update_bit_lfsr()
            next_bit_nfsr = self.update_bit_nfsr()
            y = self.function_y()
            self.shift_fsr(False,next_bit_nfsr + y)
            self.shift_fsr(True,next_bit_lfsr + y)
            self.t += 1

    def round_init_with_key(self, number_of_rounds):
        for i in range(0,number_of_rounds):
            #print(i+1)
            next_bit_lfsr = self.update_bit_lfsr()
            next_bit_nfsr = self.update_bit_nfsr()
            y = self.function_y()
            self.shift_fsr(False,next_bit_nfsr + y + self.key[(i + int(self.fsr_size/2))])
            self.shift_fsr(True,next_bit_lfsr + y + self.key[i])
            self.t += 1

    def round_(self):
        next_bit_lfsr = self.update_bit_lfsr()
        next_bit_nfsr = self.update_bit_nfsr()
        y = self.function_y()

        self.shift_fsr(False, next_bit_nfsr)
        self.shift_fsr(True, next_bit_lfsr)
        self.t += 1
        return y

    def print_init_state(self):
        print("LFSR:")
        print(f"    {self.LFSR}")
        print(f"    L:{len(self.LFSR)}")
        print("---------------------------")
        print("NFSR:")
        print(f"    {self.NFSR}")
        print(f"    L:{len(self.NFSR)}")
        print("---------------------------")

    def read_nonce(self):
        nonce = []
        with open(f'nonce.txt') as f:
            nonce = "".join(f.readlines()).replace("\n", "").replace("[","").replace("]","").split(" ")
        f.close()
        result = list(map(int, nonce))
        return result

    def read_equations(self):
        Polynomials = []
        with open(f'equations/equations_GRAIN_{self.fsr_size}_{self.no_of_rounds}.txt') as f:
          equations = f.readlines()
        f.close()
        equations = list(map(str.strip, equations[0][1:-2].strip().split(",")))
        return equations

    def generate_keystream(self,len,LSH):
        right_side = []
        with open(f'equations/keystream_GRAIN_{self.fsr_size}_{self.no_of_rounds}.txt') as f:
            right_side = f.readlines()
        f.close()
        right_side = list(map(str.strip, right_side[0][1:-2].strip().split(",")))

        poly = []
        for i in range(len):
            print(f'Counter {(i+1)}')
            polynom  = grain.round_()
            poly.append(polynom + int(right_side[i]))

        if LSH == True:
            polynoms = self.read_equations()
            for polynom in poly:
                polynoms.append(f'{polynom}')
            poly_str = polynoms
            with open(f'equations/equations_GRAIN_{self.fsr_size}_{self.no_of_rounds}.txt', "w+") as f:
                f.write('%s\n' %poly_str)
            f.close()
        else:
            poly_str = f'{poly}'
            with open(f'equations/equations_GRAIN_{self.fsr_size}_{self.no_of_rounds}.txt', "w+") as f:
                f.write('%s\n' %f'{poly_str}')
            f.close()

if __name__ == "__main__":
    startTime = time.time()

    bit_version = int(sys.argv[1])
    number_of_rounds = int(sys.argv[2])
    names = ()
    for i in range(bit_version):
        names = names + ("b"+str(i),)
    B = BooleanPolynomialRing(names = names, order='lex')
    os.system(f'python3 Grain.py {bit_version} {number_of_rounds}')
    grain = GrainPolynomial(bit_version, number_of_rounds)

    #Change to False Before LSH, first start of script must be False, the other True (bcs of writing eq to file in keystream function)
    grain.generate_keystream(16,True)
    if number_of_rounds == False:
        number_of_rounds = bit_version*3 + 2*int(bit_version/2)
    else:
        n = int(number_of_rounds/4)
        number_of_rounds = n*3 + 2*(int(n/2))

    with open('variables.txt', "w+") as f:
        f.write('%s\n' %f'{B.gens()}'[1:-1])
    f.close()
    process = psutil.Process(os.getpid())
    executionTime = (time.time() - startTime)
    with open(f'time/eq_time_GRAIN_{bit_version}_{number_of_rounds}.txt' , "w+") as f:
        f.write('%s\n' %str(executionTime))
        f.write('%s\n' %str(float(process.memory_info()[0]/ 1024 ** 2)))
    f.close()
