Táto diplomová práca sa zaoberá algebraickou kryptoanalýzou, ktorá šifru Grain-128AEADv2 prevedie na sústavu polynomiálnych rovníc a následne ju vyrieši pomocou Groebnerových báz. Algebraická kryptoanalýza bude aplikovaná na zjednodšené varianty šifry Grain.

Autor: Daniel Minarovič

ČVUT Fakulta informačních technologií
